<?php
include 'config.php';

try {
    $dbTele = new PDO($teleConfig['host'], $teleConfig['user'], $teleConfig['password']);
    $dbOauth = new PDO($oauthConfig['host'], $oauthConfig['user'], $oauthConfig['password']);
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
    exit(255);
}
// пароль и соль для пользователей в oauth
// по умолчанию у всех станет новый пароль 123456
$userPassword = 'iGXdvWN1QAjubrZ+WiIL9H8FvXHoyncRbTfXzr2/MBIPXvH5kf6X6JF97KiSK+RERcADLpkf6q0pJdBn1EfXZw==';
$salt = 'le8inrq5y3kk84oo8ckccogw4wsk0sw';

// очистим таблицу пользователей в oauth
$dbOauth->exec('DELETE FROM oauth_user');

$userQuery = 'SElECT id, email, login, name, surname, middlename FROM hc_user ';
$res = $dbTele->query($userQuery);
if (false === $res) {
    echo 'error: cant fetch users from teleradiology:' . var_export($dbTele->errorInfo(), true);
}

$fp = fopen("error_migrate_user.txt", "wb");
if(!$fp) {
    echo 'error open file';
    exit(255);
}

while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    // $row - ассоциативный массив значений, ключи - названия столбцов
    $insertQuery = 'INSERT INTO oauth_user
              (id,
              username,
              username_canonical,
              firstname,
              lastname,
              middlename,
              password,
              salt,
              email,
              email_canonical,
              roles,
              enabled
              )
              VALUES
              (:id,
              :username,
              :username_canonical,
              :firstname,
              :lastname,
              :middlename,
              :password,
              :salt,
              :email,
              :email_canonical,
              :roles,
              :enabled)';

    $roles = 'a:0:{}';
    if (in_array($row['login'], $admins)) {
        $roles = 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}';
    }

    // в sso нельзя в качестве username использовать email
    $userName = str_replace('@', '_', $row['login']);

    $query = $dbOauth->prepare($insertQuery);
    $result = $query->execute([
        ':id' => $row['id'],
        ':username' => $userName,
        ':username_canonical' => canonicalize($userName),
        ':firstname' => $row['name'],
        ':lastname' => $row['surname'],
        ':middlename' => $row['middlename'],
        ':password' => $userPassword,
        ':salt' => $salt,
        ':email' => $row['email'],
        ':email_canonical' => canonicalize($row['email']),
        ':roles' => $roles,
        ':enabled' =>  1
    ]);
    if (!$result) {

        $text = "User id " . $row['id'] . ". SQL: " . var_export($query->errorInfo(), true) . "\r\n";
        $test = fwrite($fp, $text);
        if (!$test) {
            echo 'Ошибка при записи в файл.';
            exit(255);
        }

    }
    echo 'User ' . $userName . ' migrate successfully' . "\r\n";

}

fclose($fp);
echo 'done';

function canonicalize($string)
{
    return null === $string ? null : mb_convert_case($string, MB_CASE_LOWER, mb_detect_encoding($string));
}