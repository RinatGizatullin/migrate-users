<?php

/**
 * Скрипт для очистки таблицы с пользователями
 */

include 'config.php';
try {
    $dbOauth = new PDO($oauthConfig['host'], $oauthConfig['user'], $oauthConfig['password']);
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
    exit(255);
}

// очистим таблицу пользователей в oauth
$result = $dbOauth->exec('DELETE FROM oauth_user');