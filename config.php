<?php
mb_internal_encoding("UTF-8");

/* Подключение к базе данных ODBC, используя вызов драйвера */
$teleConfig = [
    'host' => 'mysql:dbname=teleradiology;host=127.0.0.1;charset=UTF8',
    'user' => 'root',
    'password' => 'root'
];

$oauthConfig = [
    'host' => 'mysql:dbname=oauthService;host=127.0.0.1;charset=UTF8',
    'user' => 'root',
    'password' => 'root'
];

/* Перечичслить логины администраторов в телерадиологии */
$admins = ['admin@admin.com', 'admin'];